/* See LICENSE file for copyright and license details. */

/* variables */
#define TERMINAL "alacritty"

/* appearance */
static unsigned int borderpx  = 2;        /* border pixel of windows */
static const Gap default_gap  = {.isgap = 1, .realgap = 10, .gappx = 10};
static unsigned int snap      = 32;       /* snap pixel */
static int showbar            = 1;        /* 0 means no bar */
static int topbar             = 1;        /* 0 means bottom bar */
static const char *fonts[]    = { 
    "iosekva:weight=600:size=10", 
    "JoyPixels:pixelsize=10:antialias=true:autohint=true" 
};
static const char dmenufont[]       = "iosekva:weight=600:size=10";
static char normbgcolor[]           = "#222222";
static char normbordercolor[]       = "#444444";
static char normfgcolor[]           = "#bbbbbb";
static char selfgcolor[]            = "#eeeeee";
static char selbordercolor[]        = "#005577";
static char selbgcolor[]            = "#005577";
static char *colors[][3] = {
       /*               fg           bg           border   */
       [SchemeNorm] = { normfgcolor, normbgcolor, normbordercolor },
       [SchemeSel]  = { selfgcolor,  selbgcolor,  selbordercolor  },
};

/* tagging */
static const char *tags[] = { "1", "2", "3", "4", "5", "6" };

static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 */
	/* class      instance    title       tags mask     isfloating  iscentered  monitor */
	{ "Gimp",     NULL,       NULL,       0,            1,          0,         -1 },
};

/* layout(s) */
static float mfact     = 0.5; /* factor of master area size [0.05..0.95] */
static int nmaster     = 1;    /* number of clients in master area */
static int resizehints = 1;    /* 1 means respect size hints in tiled resizals */

#define FORCE_VSPLIT 1
#include "nrowgrid.c"

static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "[]=",      tile },    /* first entry is default */
	{ "><>",      NULL },    /* no layout function means floating behavior */
	{ "[M]",      monocle },
	{ "###",      nrowgrid },
};

/* key definitions */
#define MODKEY Mod4Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[] = { "dmenu_run", "-m", dmenumon, "-fn", dmenufont, "-nb", normbgcolor, "-nf", normfgcolor, "-sb", selbordercolor, "-sf", selfgcolor, NULL };
static const char *termcmd[]  = { TERMINAL, NULL };

/*
 * Xresources preferences to load at startup
 */
ResourcePref resources[] = {
		{ "dwm.color0",         STRING,  &normbgcolor },
		{ "dwm.color0",	        STRING,  &normbordercolor },
		{ "dwm.color4",         STRING,  &normfgcolor },
		{ "dwm.color4",         STRING,  &selbgcolor },
		{ "dwm.color8",         STRING,  &selbordercolor },
		{ "dwm.color0",         STRING,  &selfgcolor },
		{ "borderpx",          	INTEGER, &borderpx },
		{ "snap",          		INTEGER, &snap },
		{ "showbar",          	INTEGER, &showbar },
		{ "topbar",          	INTEGER, &topbar },
		{ "nmaster",          	INTEGER, &nmaster },
		{ "resizehints",       	INTEGER, &resizehints },
		{ "mfact",      	 	FLOAT,   &mfact },
};

#include <X11/XF86keysym.h>

static Key keys[] = {
	/* modifier                     key           function        argument */
	{ MODKEY,                       XK_d,         spawn,          {.v = dmenucmd } },
	{ MODKEY,                       XK_Return,    spawn,          {.v = termcmd } },
//	{ MODKEY,                       XK_grave,     togglescratch,  {.v = scratchpadcmd } },
	{ MODKEY,                       XK_b,         togglebar,      {0} },
//	{ MODKEY|ShiftMask,             XK_j,         rotatestack,    {.i = +1 } },
//	{ MODKEY|ShiftMask,             XK_k,         rotatestack,    {.i = -1 } },
	{ MODKEY,                       XK_j,         focusstack,     {.i = +1 } },
	{ MODKEY,                       XK_k,         focusstack,     {.i = -1 } },
	{ MODKEY,                       XK_o,         incnmaster,     {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_o,         incnmaster,     {.i = -1 } },
	{ MODKEY,                       XK_h,         setmfact,       {.f = -0.05} },
	{ MODKEY,                       XK_l,         setmfact,       {.f = +0.05} },
	{ MODKEY,                       XK_space,     zoom,           {0} },
	{ MODKEY,                       XK_Tab,       view,           {0} },
	{ MODKEY,                       XK_t,         setlayout,      {.v = &layouts[0]} },
	{ MODKEY,                       XK_y,         setlayout,      {.v = &layouts[1]} },
	{ MODKEY,                       XK_u,         setlayout,      {.v = &layouts[2]} },
	{ MODKEY,                       XK_i,         setlayout,      {.v = &layouts[3]} },
	{ MODKEY|ShiftMask,             XK_i,         setlayout,      {0} },
	{ MODKEY|ShiftMask,             XK_space,     togglefloating, {0} },
	{ MODKEY,                       XK_s,         togglesticky,   {0} },
	{ MODKEY,                       XK_f,         togglefullscr,  {0} },
	{ MODKEY,                       XK_0,         view,           {.ui = ~0 } },
	{ MODKEY|ShiftMask,             XK_0,         tag,            {.ui = ~0 } },
//	{ MODKEY|ShiftMask,             XK_comma,     tagmon,         {.i = -1 } },
//	{ MODKEY|ShiftMask,             XK_period,    tagmon,         {.i = +1 } },
	{ MODKEY,                       XK_z,         setgaps,        {.i = -5 } },
	{ MODKEY,                       XK_x,         setgaps,        {.i = +5 } },
	{ MODKEY|ShiftMask,             XK_x,         setgaps,        {.i = GAP_RESET } },
	{ MODKEY|ShiftMask,             XK_z,         setgaps,        {.i = GAP_TOGGLE} },
	{ MODKEY,                       XK_minus,     spawn,          SHCMD("pamixer -d 3; kill -44 $(pidof dwmblocks)") },
	{ MODKEY|ShiftMask,             XK_minus,     spawn,          SHCMD("pamixer -d 5; kill -44 $(pidof dwmblocks)") },
	{ MODKEY,                       XK_equal,     spawn,          SHCMD("pamixer -i 3; kill -44 $(pidof dwmblocks)") },
	{ MODKEY|ShiftMask,             XK_equal,     spawn,          SHCMD("pamixer -i 5; kill -44 $(pidof dwmblocks)") },
	{ MODKEY,                       XK_BackSpace, spawn,          SHCMD("sysact") },
	{ MODKEY,                       XK_q,         killclient,           {0} },
	{ MODKEY|ShiftMask,             XK_q,         quit,           {0} },
	{ MODKEY|ShiftMask|ControlMask, XK_q,         quit,           {1} },
	{ MODKEY|ShiftMask,             XK_h,         tagmon,         {.i = -1 } },
	{ MODKEY|Mod1Mask,              XK_h,         focusmon,       {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_l,         tagmon,         {.i = +1 } },
	{ MODKEY|Mod1Mask,              XK_l,         focusmon,       {.i = -1 } },
	{ MODKEY,                       XK_w,         spawn,          SHCMD("$BROWSER") },
	{ MODKEY,                       XK_e,         spawn,          SHCMD(TERMINAL " -e lf") },
	{ MODKEY,                       XK_r,         spawn,          SHCMD(TERMINAL " -e htop") },
	{ MODKEY,                       XK_p,         spawn,          SHCMD("playerctl play-pause") },
	{ MODKEY|ShiftMask,             XK_p,         spawn,          SHCMD("playerctl pause ; mpv-cli -pA") },
	{ MODKEY|ControlMask,           XK_p,         spawn,          SHCMD("mpv-cli -pA") },
	{ MODKEY|ShiftMask,             XK_n,         spawn,          SHCMD(TERMINAL " -e newsboat; pkill -RTMIN+6 dwmblocks") },
	{ MODKEY|ShiftMask,             XK_m,         spawn,          SHCMD("pamixer -t; kill -44 $(pidof dwmblocks)") },
	{ MODKEY,                       XK_comma,     spawn,          SHCMD("playerctl previous") },
	{ MODKEY,                       XK_period,    spawn,          SHCMD("playerctl next") },
	{ MODKEY,                       XK_F3,        spawn,          SHCMD("displayselect") },
	{ MODKEY,                       XK_F11,       spawn,          SHCMD("mpv --no-cache --no-osc --no-input-default-bindings --input-conf=/dev/null --title=webcam $(ls /dev/video[0,2,4,6,8] | tail -n 1)") },
	{ 0,                            XK_Print,     spawn,          SHCMD("maim pic-full-$(date '+%y%m%d-%H%M-%S').png") },
	{ ShiftMask,                    XK_Print,     spawn,          SHCMD("maimpick") },
	{ 0, XF86XK_AudioMute,                        spawn,          SHCMD("pamixer -t; kill -44 $(pidof dwmblocks)") },
	{ 0, XF86XK_AudioRaiseVolume,                 spawn,          SHCMD("pamixer -i 1; kill -44 $(pidof dwmblocks)") },
	{ 0, XF86XK_AudioLowerVolume,                 spawn,          SHCMD("pamixer -d 1; kill -44 $(pidof dwmblocks)") },
	{ 0, XF86XK_AudioPrev,                        spawn,          SHCMD("playerctl previous") },
	{ 0, XF86XK_AudioNext,                        spawn,          SHCMD("playerctl next") },
	{ 0, XF86XK_AudioPause,                       spawn,          SHCMD("playerctl pause") },
	{ 0, XF86XK_AudioPlay,                        spawn,          SHCMD("playerctl play-pause") },
	{ 0, XF86XK_AudioStop,                        spawn,          SHCMD("playerctl stop") },
	{ 0, XF86XK_AudioMedia,                       spawn,          SHCMD(TERMINAL " -e ncmpcpp") },
	{ 0, XF86XK_AudioMicMute,                     spawn,          SHCMD("pactl set-source-mute @DEFAULT_SOURCE@ toggle") }, 
	{ 0, XF86XK_PowerOff,                         spawn,          SHCMD("sysact") },
	{ 0, XF86XK_Calculator,                       spawn,          SHCMD(TERMINAL " -e bc -l") },
	{ 0, XF86XK_Sleep,                            spawn,          SHCMD("sudo -A zzz") },
	{ 0, XF86XK_WWW,                              spawn,          SHCMD("$BROWSER") },
	{ 0, XF86XK_DOS,                              spawn,          SHCMD(TERMINAL) },
	{ 0, XF86XK_ScreenSaver,                      spawn,          SHCMD("slock & xset dpms force off; mpc pause; mpv-cli -pA") },
	{ 0, XF86XK_TaskPane,                         spawn,          SHCMD(TERMINAL " -e htop") },
	{ 0, XF86XK_Mail,                             spawn,          SHCMD(TERMINAL " -e neomutt ; pkill -RTMIN+12 dwmblocks") },
	{ 0, XF86XK_MyComputer,                       spawn,          SHCMD(TERMINAL " -e lf /") },
	/* { 0, XF86XK_Battery,                       spawn,          SHCMD("") }, */
	{ 0, XF86XK_Launch1,                          spawn,          SHCMD("xset dpms force off") },
	{ 0, XF86XK_TouchpadToggle,                   spawn,          SHCMD("(synclient | grep 'TouchpadOff.*1' && synclient TouchpadOff=0) || synclient TouchpadOff=1") },
	{ 0, XF86XK_TouchpadOff,                      spawn,          SHCMD("synclient TouchpadOff=1") },
	{ 0, XF86XK_TouchpadOn,                       spawn,          SHCMD("synclient TouchpadOff=0") },
	{ 0, XF86XK_MonBrightnessUp,                  spawn,          SHCMD("xbacklight -inc 15") },
	{ 0, XF86XK_MonBrightnessDown,                spawn,          SHCMD("xbacklight -dec 15") },
	TAGKEYS(                        XK_1,                         0)
	TAGKEYS(                        XK_2,                         1)
	TAGKEYS(                        XK_3,                         2)
	TAGKEYS(                        XK_4,                         3)
	TAGKEYS(                        XK_5,                         4)
	TAGKEYS(                        XK_6,                         5)
	TAGKEYS(                        XK_7,                         6)
	TAGKEYS(                        XK_8,                         7)
	TAGKEYS(                        XK_9,                         8)
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
	{ ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },
	{ ClkWinTitle,          0,              Button2,        zoom,           {0} },
	{ ClkStatusText,        0,              Button2,        spawn,          {.v = termcmd } },
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};

